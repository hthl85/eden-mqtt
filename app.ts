import { EdenMongo } from './lib/mongo';
import { EdenBroker } from './lib/mosca';

let ascoltatore = {
  type: 'redis',
  redis: require('redis'),
  db: 12,
  port: 6379,
  host: process.env.REDIS_HOST
};

let settings = {
  port: process.env.NODE_PORT || 4883,
  backend: ascoltatore
};

let emongo = new EdenMongo();
let emosca = new EdenBroker(settings);

emosca.broker.on('published', function (packet, client) {
  if (packet.topic.indexOf('$SYS') === 0) {
    return; // doesn't print stats info
  }
  console.log('ON PUBLISHED', packet.payload.toString(), 'on topic', packet.topic);
});

emosca.broker.on('ready', function () {
  console.log('MQTT Server listening on port');
});