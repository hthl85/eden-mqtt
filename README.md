# Eden MQTT Broker

## Start project from scratch

### TypeScript with NodeJS ([link](https://basarat.gitbooks.io/typescript/content/docs/quick/nodejs.html))

- Setup a nodejs project `package.json`. Quick one : `npm init -y`   
- Add TypeScript (`npm install typescript --save-dev`)   
- Add `node.d.ts` (`npm install @types/node --save-dev`)   
- Init a `tsconfig.json` for TypeScript options (`node ./node_modules/.bin/tsc --init`)

### Bonus: Live compile + run ([link](https://basarat.gitbooks.io/typescript/content/docs/quick/nodejs.html))

- Add `ts-node` which we will use for live compile + run in node (`npm install ts-node --save-dev`)   
- Add `nodemon` which will invoke `ts-node` whenever a file is changed (`npm install nodemon --save-dev`)   

Now just add a `script` target to your `package.json` based on your application entry e.g. assuming its `app.ts`: 
```json
"scripts": {
    "start": "npm run build:live",
    "build:live": "nodemon --exec ./node_modules/.bin/ts-node -- ./app.ts"
  }
```

So you can now run `npm start` and as you edit `app.ts`:   
- `nodemon` rereuns its command (`ts-node`)
- `ts-node` transpiles automatically picking up `tsconfig.json` and the installed typescript version
- `ts-node` runs the output javascript through node.