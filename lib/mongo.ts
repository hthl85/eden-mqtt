import * as mongoose from 'mongoose';

export class EdenMongo {
  constructor() {
    // Default promise library 
    // (http://mongoosejs.com/docs/promises.html)
    (<any>mongoose).Promise = global.Promise;

    // Set connection string
    let uri = 'mongodb://localhost/mean-dev';

    // Connect to mongo server
    mongoose.connect(uri, (err) => {
      if (err) {
        console.log(err.message);
        console.log(err);
      }
      else {
        console.log('Connected to MongoDb');
      }
    });
  }
}