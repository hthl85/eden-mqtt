import { Authorizer } from './authorizer';

let mosca = require('mosca');

export class EdenBroker {

  public broker: any;
  private auth: Authorizer;

  constructor(private settings: any) {
    this.auth = new Authorizer();
    this.broker = new mosca.Server(settings, this.done)
    this.broker.authenticate = this.auth.authenticate;
    this.broker.authorizePublish = this.auth.authorizePublish;
    this.broker.authorizeSubscribe = this.auth.authorizeSubscribe;
  }

  private done() {
    console.log('Done in EdenBroker is called.');
  }
}
