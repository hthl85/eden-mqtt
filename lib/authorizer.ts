import { Devices } from '../model/device';

export class Authorizer {
  authenticate(client, username, password, callback) {
    Devices.findOne({
      user: username,
      id: client.id,
      secret: password.toString('utf8')
    }, function (err, doc) {
      if (doc) client.user = username;
      callback(null, doc);
    });
  }

  authorizePublish(client, topic, payload, callback) {
    callback(null, client.user == topic.split('/')[1]);
  }

  authorizeSubscribe(client, topic, callback) {
    callback(null, client.user == topic.split('/')[1]);
  }
}