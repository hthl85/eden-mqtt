import * as mongoose from 'mongoose';

export interface IDeviceModel extends mongoose.Document {
  id: string,
  name: string,
  desc: string,
  subTopic: string,
  pubTopic: string,
  created: Date,
  user: mongoose.Schema.Types.ObjectId
}

/**
 * Devices Schema
 * 
 * NOTE: I am asuming we don't need validation b/c 
 * we only retrieve data
 */
let DevicesSchema = new mongoose.Schema({
  id: String,
  name: String,
  desc: String,
  subTopic: String,
  pubTopic: String,
  created: Date,
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
});

export let Devices = mongoose.model<IDeviceModel>('device', DevicesSchema);